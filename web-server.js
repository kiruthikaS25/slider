// require dependencies
var express=require('express');
var path=require('path');
//configure and create app
var app = express();
var port=8001;
// static path
app.use( express.static(path.join(__dirname,'/webapps')))
app.use( express.static(path.join(__dirname,'/pages')))
app.use(express.urlencoded())
// configure the routes
app.get("/",function(req,res){
    res.render("\pages\index.html");
});
//start the server
app.listen(port,function(){
    console.log("server running on this port " + port + ".....");
})  